# CI/CD Pipeline for LaTeX Projects 

The ci/cd pipelines provided by GitLab can be used to compile LaTeX documents and thus sparing local resources while always providing an up-to-date version of the compiled document. 

This repo serves as a blueprint to first build a minimal Docker image for compilation, which is saved in the repositories Container Registry, and second compiling the document on a new push. 


## Improvements 

An improvement could be a switch to control the compilations, e.g. via `only` or `except` - based on the strategy. 

Another option would be to commit the newly created document into the repository. Thus, with each pull the user would get an up-to-date version of the document without investing any compute. Similar to the workflow in [overleaf.com](https://www.overleaf.com/), just with one's own setup!
